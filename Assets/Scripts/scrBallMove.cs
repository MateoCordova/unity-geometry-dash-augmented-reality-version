﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrBallMove : MonoBehaviour
{
    public Rigidbody rigid = new Rigidbody();
    public bool grounded;
    // Start is called before the first frame update
    void Start()
    {
        grounded = true;
        rigid.velocity = new Vector3(0, 0, 5);
    }

    // Update is called once per frame
    void Update()
    {
        if((Input.touches.Length > 0 || Input.GetMouseButton(0)) && grounded)
        {
            jump();
        } else
        {
            if (gameObject.transform.position.y < 0.5)
            {
                grounded = true;
            }
        }
        
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag.Equals("Enemy"))
        {
            gameObject.transform.position = new Vector3(0, 0, 0);
        }
        if (collision.tag.Equals("Ground"))
        {
            grounded = true;
        }
    }

    private void jump()
    {
        grounded = false;
        rigid.velocity = new Vector3(0, 7, 5);
    }
}
