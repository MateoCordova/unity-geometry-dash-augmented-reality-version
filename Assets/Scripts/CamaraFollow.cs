﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraFollow : MonoBehaviour
{
    public GameObject player;
    public int offSetX;
    public int offSetY;
    public int offSetZ;


    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = player.transform.position + new Vector3(offSetX, offSetY, offSetZ);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = player.transform.position + new Vector3(offSetX, offSetY, offSetZ);
    }
}
